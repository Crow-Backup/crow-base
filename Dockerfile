FROM maven:3-eclipse-temurin-22

RUN apt-get update && \
    apt-get install -y --no-install-recommends libopenjfx-jni fakeroot binutils libc6 zlib1g gpg lsb-release \
	&& apt-get clean && rm -rf /var/lib/apt/lists/

RUN curl -fsSL https://deb.nodesource.com/setup_16.x -o nodesource_setup.sh && \
    bash nodesource_setup.sh && \
    apt-get install -y nodejs \
    && apt-get clean && rm -rf /var/lib/apt/lists/
RUN apt-get update && \
    apt-get install -y --no-install-recommends nodejs \
	&& apt-get clean && rm -rf /var/lib/apt/lists/
